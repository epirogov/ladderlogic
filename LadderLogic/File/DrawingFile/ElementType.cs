﻿namespace LadderLogic.File.DrawingFile
{
	public enum ElementType
	{
		None,


		Coil,


		Cursor,


		Latch,


		Line,


		NcContact,


		NoContact,


		NotCoil,


		OffTimer,


		OnTimer,


		PulseTimer,


		Properties,


		Create,


		Upload
	}
}

