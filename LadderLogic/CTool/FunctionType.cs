﻿namespace LadderLogic.CTool
{
	public enum FunctionType
	{
		None,


		Error,


		In,


		Out,


		InNot,


		OutNot,


		AndBit,


		OrBit,


		XorBit,


		AndNotBit,


		OrNotBit,


		InAnalog,


		OutPwm,


		OutServo,


		Latch,


		LatchKey,


		TimerOn,


		TimerOff,


		TimerPulse
	}
}

